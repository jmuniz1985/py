
class Shark:
    def swim(self):
        print("The shark is swimming.")

    def be_awesome(self):
        print("The shark is being awesome.")



def main():
    sammy = Shark()
    sammy.swim()
    sammy.be_awesome()

if __name__ == "__main__":
    main()




class Shark:
    def __init__(self, name):
        print("This is the constructor method.")
        self.name = name

    def swim(self):
        print(self.name + " is swimming. Constructor method")

    def be_awesome(self):
        print(self.name + " is being awesome. Constructor method")

def main():
    sammy = Shark("Sammy")
    sammy.be_awesome()
    stevie = Shark("Stevie")
    stevie.swim()

if __name__ == "__main__":
  main()
