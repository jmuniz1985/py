#Creating PY Functions
#with parameters
def label(txt):
    print('-' * 50)
    print('txt')
    print('-' * 50)


#main
label( 'Machine ECommerce')


#without params
def line():
    print('-' * 50)


# main
line()
print('i')
line()
print('Store')

#exceptions handler
x = 10
if x > 5:

    raise Exception('x should not exceed 5. The value of x was: {}'.format(x))

#https://realpython.com/python-exceptions/


